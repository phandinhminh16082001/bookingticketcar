import { Fragment } from "react";
import { Routes, Route } from "react-router-dom";
import routes from "./config/routes";
import MainLayout from "./layouts/MainLayout";
import "./i18n/i18n";
import "./assets/styles/base.scss";
function App() {
  return (
    <div className="App">
      <Routes>
        {routes.map((route, index) => {
          let Layout = MainLayout;
          if (route.layout) {
            Layout = route.layout;
          } else {
            Layout = Fragment;
          }
          return (
            <Route
              key={index}
              path={route.path}
              element={<Layout>{route.element}</Layout>}
            />
          );
        })}
      </Routes>
    </div>
  );
}

export default App;
