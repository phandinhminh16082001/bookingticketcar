import * as Yup from "yup";

export const initialValues = {
  // ho: "",
  // ten: "",
  email: "",
  password: "",
  // confirmPassword: "",
  // job: "",
  // address: "",
};

export const validationSchema = Yup.object().shape({
  // ho: Yup.string().required("Bắt buộc!"),
  // ten: Yup.string().required("Bắt buộc!"),
  email: Yup.string().email("Email không hợp lệ!").required("Bắt buộc!"),
  password: Yup.string().required("Bắt buộc!"),
  // confirmPassword: Yup.string()
  //   .oneOf([Yup.ref("password")], "Mật khẩu không trùng khớp!")
  //   .required("Bắt buộc!"),
  // job: Yup.string().required("Bắt buộc!"),
  // address: Yup.string().required("Bắt buộc!"),
});
