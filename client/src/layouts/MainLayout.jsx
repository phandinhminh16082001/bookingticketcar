import React from "react";
import Footer from "../components/common/Footer/Footer";
import Header from "../components/common/Header/Header";
import Phone from "../components/common/phone-animation/Phone";

const MainLayout = (props) => {
  return (
    <div>
      <Phone />
      <Header />
      <div>{props.children}</div>
      <Footer />
    </div>
  );
};

export default MainLayout;
