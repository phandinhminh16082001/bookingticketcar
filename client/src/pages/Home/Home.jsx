import React from "react";
import classNames from "classnames/bind";
import styles from "./Home.module.scss";
import banner from "../../assets/images/banner-about-1.jpg";
import Slide from "../../components/common/Slide/Slide";
import { RxDoubleArrowRight } from "react-icons/rx";
import { ImLocation } from "react-icons/im";
import { BiTimeFive } from "react-icons/bi";
import { IoMdPricetags } from "react-icons/io";
import { slide_locations, slide_news } from "../../config/slide.config";
import { GoLocation } from "react-icons/go";
import { Link } from "react-router-dom";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Button from "../../components/common/Button/Button";
import { listTour, more_info, quality } from "../../config/info_us.config";
import Search from "../../components/features/Search/Search";
import SlidePrize from "../../components/common/SlidePrize/SlidePrize";

const cx = classNames.bind(styles);
const Home = () => {
  return (
    <div>
      <div className={cx("banner")}>
        <img src={banner} alt="" />
      </div>
      <Search />
      <SlidePrize />
      <div className={cx("common-routes")}>
        <div className={cx("container")}>
          <h3>TUYẾN PHỔ BIẾN</h3>
          <div className={cx("content")}>
            {listTour.map((item, index) => (
              <Link to="#" key={index}>
                <div className={cx("item")}>
                  <div className={cx("img")}>
                    <img src={item.img} alt="" />
                  </div>
                  <div className={cx("text")}>
                    <p className={cx("title")}>
                      {item.from}
                      <RxDoubleArrowRight /> {item.to}
                    </p>
                    <div className={cx("detail")}>
                      <div className={cx("distance")}>
                        <ImLocation />
                        {item.distance}
                      </div>
                      <div className={cx("time")}>
                        <BiTimeFive />
                        {item.time}
                      </div>
                      <div className={cx("price")}>
                        <IoMdPricetags />
                        {item.price}
                      </div>
                    </div>
                  </div>
                </div>
              </Link>
            ))}
          </div>
        </div>
      </div>
      <div className={cx("quality")}>
        <div className={cx("container")}>
          <h1>FUTA - Chất lượng là Danh dự</h1>
          <div className={cx("content")}>
            {quality.map((item, index) => (
              <div className={cx("item")} key={index}>
                <div className={cx("img")}>{item.img}</div>
                <div className={cx("text")}>
                  <div className={cx("title")}>{item.title}</div>
                  <div className={cx("sub")}>{item.sub}</div>
                  <div className={cx("desc")}>{item.desc}</div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
      <div className={cx("slide-locations")}>
        <h1 className={cx("slide-locations__title")}>Điểm đến phổ biến</h1>
        <p className={cx("slide-locations__subtitle")}>
          Gợi ý những điểm du lịch được ưa thích trong năm
        </p>
        <Slide slidesToShow={5} centerMode={true} className="center">
          {slide_locations.map((item, index) => (
            <Link key={index}>
              <div className="slide-locations__item">
                <img src={item.img} alt="" />
                <div className={cx("slide-locations__item__address")}>
                  <p className={cx("location")}>
                    <GoLocation />
                    {item.location}
                  </p>
                  <p className={cx("name")}>{item.name}</p>
                </div>
              </div>
            </Link>
          ))}
        </Slide>
      </div>
      <div className={cx("container")}>
        <div className={cx("slide-news")}>
          <h1 className={cx("title")}>tin tức cập nhật</h1>
          <p className={cx("description")}>
            Tin tức mới nhất về FUTA Lines và thông tin các tuyến xe
          </p>
          <div className={cx("slide-news__list")}>
            <Slide slidesToShow={3}>
              {slide_news.map((item, index) => (
                <Link key={index} to={item.path}>
                  <div className={cx("slide-news__list__item")}>
                    <img src={item.img} alt="" />
                    <div></div>
                    <p className={cx("slide-news__list__item__title")}>
                      {item.title}
                    </p>
                  </div>
                </Link>
              ))}
            </Slide>
          </div>
          <div className={cx("button")}>
            <Button color="white" size="sm" text="xem thêm" />
          </div>
        </div>
        <div className={cx("more-infos")}>
          {more_info.map((item, index) => (
            <Link key={index} to="#">
              <div className={cx("item")}>
                <div className={cx("img")}>{item.img}</div>
                <div className={cx("text")}>
                  <h2 className={cx("title")}>{item.title}</h2>
                  <p className={cx("desc")}>{item.desc}</p>
                </div>
              </div>
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Home;
