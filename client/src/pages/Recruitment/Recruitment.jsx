import React, { useCallback, useEffect, useState } from "react";
import classNames from "classnames/bind";
import styles from "./Recruitment.module.scss";
import { Link } from "react-router-dom";

import tuyendung from "../../assets/images/tuyendung.jpg";
import Select from "../../components/common/Select/Select";
import Button from "../../components/common/Button/Button";
import { MdWork, MdLocationPin } from "react-icons/md";
import {
  FaUsers,
  FaUser,
  FaBuilding,
  FaClock,
  FaDollarSign,
  FaCalendarAlt,
} from "react-icons/fa";

const cx = classNames.bind(styles);

const searchConfigs = [
  {
    icon: <MdWork />,
    title: "Tên công việc",
    name: "work",
    placeholder: "---Chọn công việc---",
  },
  {
    icon: <FaUsers />,
    title: "Cấp bậc",
    name: "position",
    placeholder: "---Chọn cấp bậc---",
  },
  {
    icon: <FaUser />,
    title: "Hình thức",
    name: "kind",
    placeholder: "---Chọn hình thức---",
  },
  {
    icon: <FaClock />,
    title: "Số năm kinh nghiệm",
    name: "experience",
    placeholder: "---Chọn số năm kinh nghiệm---",
  },
  {
    icon: <MdLocationPin />,
    title: "Tỉnh/TP",
    name: "location",
    placeholder: "---Chọn tỉnh/tp---",
  },
  {
    icon: <FaBuilding />,
    title: "Công ty",
    name: "company",
    placeholder: "---Chọn công ty---",
  },
];

const fakeData = [
  {
    title: "Lái xe tuyến",
    location: "Đà Nẵng",
    company: "BusLines",
    income: "9-10tr",
    deadline: "09/04/2023",
    experience: "dưới 1 năm",
    kind: "Nhân viên chính thức",
    position: "Nhân viên",
  },
  {
    title: "Lái xe tuyến",
    location: "Quy Nhơn",
    company: "Mart",
    income: "9-10tr",
    deadline: "09/04/2023",
    experience: "2-3 năm",
    kind: "Nhân viên thời vụ",
    position: "Nhân viên",
  },
  {
    title: "Nhân viên bán vé",
    location: "Quy Nhơn",
    company: "Mart",
    income: "9-10tr",
    deadline: "09/04/2023",
    experience: "2-3 năm",
    kind: "Nhân viên thời vụ",
    position: "Nhân viên",
  },
  {
    title: "Thủ kho",
    location: "Gia Lai",
    company: "Express",
    income: "7-8tr",
    deadline: "30/04/2023",
    experience: "trên 3 năm",
    kind: "Nhân viên bán thời gian",
    position: "Thực tập sinh",
  },
];

const configOption = {
  work: [
    { name: "Lái xe tuyến", value: "Lái xe tuyến" },
    { name: "Nhân viên bán vé", value: "Nhân viên bán vé" },
    { name: "Thủ kho", value: "Thủ kho" },
  ],
  position: [
    { name: "Thực tập sinh", value: "Thực tập sinh" },
    { name: "Nhân viên", value: "Nhân viên" },
    { name: "Quản lý", value: "Quản lý" },
  ],
  kind: [
    { name: "Nhân viên chính thức", value: "Nhân viên chính thức" },
    { name: "Nhân viên thời vụ", value: "Nhân viên thời vụ" },
    { name: "Nhân viên bán thời gian", value: "Nhân viên bán thời gian" },
  ],
  experience: [
    { name: "dưới1 năm", value: "dưới 1 năm" },
    { name: "2-3 năm", value: "2-3 năm" },
    { name: "trên 3 năm", value: "trên 3 năm" },
  ],
  location: [
    { name: "Gia Lai", value: "Gia Lai" },
    { name: "Đà Nẵng", value: "Đà Nẵng" },
    { name: "Quy Nhơn", value: "Quy Nhơn" },
  ],
  company: [
    { name: "BusLines", value: "BusLines" },
    { name: "Express", value: "Express" },
    { name: "Mart", value: "Mart" },
  ],
};
const Recruitment = () => {
  const initialFilter = {
    work: "",
    position: "",
    kind: "",
    experience: "",
    location: "",
    company: "",
  };

  const [filter, setFilter] = useState(initialFilter);
  const [listJobs, setListJobs] = useState(fakeData);
  const filterSelect = (type, value) => {
    switch (type) {
      case "work":
        setFilter({ ...filter, work: value });
        break;
      case "position":
        setFilter({ ...filter, position: value });
        break;
      case "kind":
        setFilter({ ...filter, kind: value });
        break;
      case "time":
        setFilter({ ...filter, time: value });
        break;
      case "location":
        setFilter({ ...filter, location: value });
        break;
      case "company":
        setFilter({ ...filter, company: value });
        break;

      default:
        break;
    }
  };
  const listJob = useCallback(() => {
    let temp = fakeData;
    if (filter.work) {
      temp = temp.filter((item) => item.title.includes(filter.work));
    }
    if (filter.position) {
      temp = temp.filter((item) => item.position.includes(filter.position));
    }
    if (filter.kind) {
      temp = temp.filter((item) => item.kind.includes(filter.kind));
    }
    if (filter.experience) {
      temp = temp.filter((item) => item.experience.includes(filter.experience));
    }
    if (filter.location) {
      temp = temp.filter((item) => item.location.includes(filter.location));
    }
    if (filter.company) {
      temp = temp.filter((item) => item.company.includes(filter.company));
    }

    setListJobs(temp);
  }, [filter]);

  useEffect(() => {
    listJob();
  }, [listJob]);
  return (
    <div className={cx("container")}>
      <div className={cx("recruitment")}>
        <img src={tuyendung} alt="tuyendung" />

        <div className={cx("search-control")}>
          {searchConfigs.map((item, index) => (
            <div className={cx("item")} key={index}>
              <p className={cx("title")}>
                <span>{item.icon}</span>
                <span>{item.title}</span>
              </p>
              <Select
                name={item.name}
                placeholder={item.placeholder}
                options={configOption[item.name]}
                onChange={(value) => filterSelect(item.name, value)}
              />
            </div>
          ))}
        </div>

        <div className={cx("search-btn")}>
          <Button
            color="orange"
            text="Tìm kiếm"
            size="lg"
            onClick={() => listJob()}
          />
        </div>
        <hr />

        <div className={cx("list")}>
          {listJobs.map((item, index) => (
            <div className={cx("item")} key={index}>
              <Link to="#">
                <div className={cx("content")}>
                  <div className={cx("info")}>
                    <p className={cx("title")}>{item.title}</p>
                    <div className={cx("detail")}>
                      <p className={cx("company")}>
                        <span className={cx("icon")}>
                          <FaBuilding />
                        </span>
                        <span>{item.company}</span>
                      </p>
                      <p className={cx("location")}>
                        <span className={cx("icon")}>
                          <MdLocationPin />
                        </span>
                        <span>{item.location}</span>
                      </p>
                      <p className={cx("income")}>
                        <span className={cx("icon")}>
                          <FaDollarSign />
                        </span>
                        <span>{item.income}</span>
                      </p>
                    </div>
                  </div>
                  <div className={cx("deadline")}>
                    <p className={cx("title")}>Hạn nộp</p>
                    <p className={cx("date")}>
                      <span className={cx("icon")}>
                        <FaCalendarAlt />
                      </span>
                      <span>{item.deadline}</span>
                    </p>
                  </div>
                </div>
              </Link>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Recruitment;
