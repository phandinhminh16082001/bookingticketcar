import React from "react";
import classNames from "classnames/bind";
import styles from "./History.module.scss";
import Button from "../../../components/common/Button/Button";
import InputSearch from "../../../components/common/InputSearch/InputSearch";
import InputDate from "../../../components/common/InputDate/InputDate";
import Select from "../../../components/common/Select/Select";
import { RxDoubleArrowRight } from "react-icons/rx";

const cx = classNames.bind(styles);

const History = () => {
  return (
    <div className={cx("container")}>
      <div className={cx("history")}>
        <div className={cx("heading")}>
          <h2 className={cx("heading-title")}>Lịch sử mua vé</h2>
          <div className={cx("heading-btn")}>
            <Button color="orange" size="lg" text="đặt vé" />
          </div>
        </div>
        <div className={cx("search")}>
          <div className={cx("item")}>
            <p className={cx("title")}>Từ khoá</p>
            <InputSearch size="sm" placeHolder="nhập mã vé" showIcon={false} />
          </div>
          <div className={cx("item")}>
            <p className={cx("title")}>Từ khoá</p>
            <InputDate />
          </div>
          <div className={cx("item")}>
            <p className={cx("title")}>Tuyến đường</p>
            <Select
              name="route"
              options={[1, 2, 3]}
              placeholder="Chọn tuyến đường"
              size="sm"
            />
          </div>
          <div className={cx("item")}>
            <p className={cx("title")}>Trạng thái</p>
            <Select
              name="status"
              options={[1, 2, 3]}
              placeholder="Chọn trạng thái"
              size="sm"
            />
          </div>
          <div className={cx("item", "item-btn")}>
            <Button color="white-black" text="tìm" />
            <Button color="white-orange" text="Làm mới" />
          </div>
        </div>
        <table className={cx("history-table")}>
          <thead>
            <tr>
              <th>Mã</th>
              <th>SL</th>
              <th>Tuyến đường</th>
              <th>Ngày đi</th>
              <th>Giờ</th>
              <th>Tổng tiền</th>
              <th>Thanh toán</th>
              <th>Trạng thái</th>
              <th>Thao tác</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className={cx("code")}>08APOX</td>
              <td className={cx("total")}>1</td>
              <td className={cx("route")}>
                Da Nang <RxDoubleArrowRight /> Da Lat
              </td>
              <td className={cx("date")}>23/03/2023</td>
              <td className={cx("time")}>13:30</td>
              <td className={cx("totlPrice")}>340000</td>
              <td className={cx("payment")}>ShoppePay</td>
              <td className={cx("status")}>Wait</td>
              <td className={cx("operation")}></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default History;
