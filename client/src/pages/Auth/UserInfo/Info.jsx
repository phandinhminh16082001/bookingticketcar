import React, { useState } from "react";
import * as Yup from "yup";

import { FastField, Form, Formik } from "formik";
import classNames from "classnames/bind";
import styles from "./Info.module.scss";
import TextBox from "../../../components/common/TextBox/TextBox";
import RadioButton from "../../../components/common/RadioButton/RadioButton";
import { LocalizationProvider } from "@mui/x-date-pickers";

import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { DemoContainer } from "@mui/x-date-pickers/internals/demo";
import moment from "moment";
import Button from "../../../components/common/Button/Button";
import { AiOutlinePlus } from "react-icons/ai";
import { MdOutlineCancel } from "react-icons/md";
import Select from "../../../components/common/Select/Select";

const cx = classNames.bind(styles);

const Info = () => {
  const [update, setUpdate] = useState(true);
  const initialValues = {
    name: "",
    email: "",
    phone: "0912344567",
    idcard: "",
    sex: "",
    date: "",
    job: "",
    address: "",
    province: "",
    district: "",
    ward: "",
  };
  const validationSchema = Yup.object().shape({
    name: Yup.string().required("Bắt buộc!"),
    email: Yup.string().email("Email không hợp lệ!").required("Bắt buộc!"),
    phone: Yup.string().required("Bắt buộc!"),
    idcard: Yup.string().required("Bắt buộc!"),
    sex: Yup.string().required("Bắt buộc!"),
    date: Yup.string().required("Bắt buộc!"),
    job: Yup.string().required("Bắt buộc!"),
    address: Yup.string().required("Bắt buộc!"),
    province: Yup.string().required("Bắt buộc!"),
    district: Yup.string().required("Bắt buộc!"),
    ward: Yup.string().required("Bắt buộc!"),
  });
  return (
    <div className={cx("container")}>
      <div className={cx("info")}>
        <h1>Thông tin cá nhân</h1>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={(values) => console.log("submit: ", values)}
        >
          {({ setFieldValue, values }) => {
            console.log(values);
            return (
              <Form>
                <div className={cx("content")}>
                  <div className={cx("left")}>
                    <p className={cx("title")}>Thông tin cá nhân</p>
                    <div className={cx("avatar")}>
                      <input
                        type="file"
                        name="upload-avatar"
                        id=""
                        className={cx("input-avatar")}
                        disabled={update}
                      />
                      <img
                        src="https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_960_720.png"
                        alt="avatar"
                      />
                    </div>

                    <div className={cx("item")}>
                      <label htmlFor="">Họ và tên</label>
                      {update ? (
                        <p>Khoa</p>
                      ) : (
                        <FastField
                          name="name"
                          component={TextBox}
                          placeholder="Nhập họ và tên"
                          defaultValue="Khoa"
                        />
                      )}
                    </div>
                    <div className={cx("item")}>
                      <label htmlFor="">Email</label>
                      {update ? (
                        <p>dangkhoa@gmail.com</p>
                      ) : (
                        <FastField
                          name="email"
                          component={TextBox}
                          placeholder="Nhập email"
                        />
                      )}
                    </div>
                    <div className={cx("item")}>
                      <label htmlFor="">Số điện thoại</label>
                      {update ? (
                        <p>0912344567</p>
                      ) : (
                        <FastField
                          name="phone"
                          component={TextBox}
                          placeholder="Nhập số điện thoại"
                        />
                      )}
                    </div>
                    <div className={cx("item")}>
                      <label htmlFor="">CMND/CCCD</label>
                      {update ? (
                        <p>Chưa cập nhật</p>
                      ) : (
                        <FastField
                          name="idcard"
                          component={TextBox}
                          placeholder="CMND/CCCD"
                        />
                      )}
                    </div>
                    <div className={cx("item")}>
                      <label htmlFor="">Giới tính</label>
                      {update ? (
                        <p>Chưa cập nhật</p>
                      ) : (
                        <>
                          <FastField
                            name="sex"
                            value="nam"
                            component={RadioButton}
                            text="Nam"
                            checked={true}
                          />
                          <FastField
                            name="sex"
                            value="nu"
                            component={RadioButton}
                            text="Nữ"
                          />
                        </>
                      )}
                    </div>
                    <div className={cx("item")}>
                      <label htmlFor="">Ngày sinh</label>
                      {update ? (
                        <p>Chưa cập nhật</p>
                      ) : (
                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                          <DemoContainer components={["DatePicker"]}>
                            <DatePicker
                              sx={{
                                "& .css-o9k5xi-MuiInputBase-root-MuiOutlinedInput-root":
                                  {
                                    fontSize: 14,
                                  },
                                "& .css-nxo287-MuiInputBase-input-MuiOutlinedInput-input":
                                  {
                                    padding: "5px 10px",
                                  },
                                "& .css-1xhypcz-MuiStack-root": {
                                  paddingTop: "0px",
                                },
                              }}
                              name="date"
                              onChange={(value) =>
                                setFieldValue(
                                  "date",
                                  moment(new Date(value)).format("DD/MM/YYYY")
                                )
                              }
                              format="DD/MM/YYYY"
                            />
                          </DemoContainer>
                        </LocalizationProvider>
                      )}
                    </div>
                    <div className={cx("item")}>
                      <label htmlFor="">Nghề nghiệp</label>
                      {update ? (
                        <p>Chưa cập nhật</p>
                      ) : (
                        <FastField
                          name="job"
                          component={TextBox}
                          placeholder="Nhập nghề nghiệp"
                        />
                      )}
                    </div>
                  </div>
                  <div className={cx("right")}>
                    <p className={cx("title")}>Thông tin cá nhân</p>
                    <div className={cx("btn-address")}>
                      <Button
                        color="orange-secondary"
                        size="block"
                        text={update ? "Thêm địa chỉ" : "Huỷ"}
                        icon={update ? <AiOutlinePlus /> : <MdOutlineCancel />}
                        onClick={() => setUpdate(!update)}
                      />
                    </div>
                    <div
                      className={cx(
                        "right-content",
                        `${update ? "active" : ""}`
                      )}
                    >
                      <div className={cx("item")}>
                        <label htmlFor="">Địa chỉ</label>
                        <FastField
                          name="address"
                          component={TextBox}
                          placeholder="Nhập địa chỉ"
                        />
                      </div>
                      <div className={cx("item")}>
                        <label htmlFor="">Tỉnh thành</label>
                        <Select
                          name="tinh"
                          options={[
                            { name: "Đà nẵng", value: "Da-Nang" },
                            { name: "Quy nhơn", value: "Quy-Nhon" },
                          ]}
                          onChange={(value) => setFieldValue("province", value)}
                        />
                      </div>
                      <div className={cx("item")}>
                        <label htmlFor="">Quận/Huyện</label>
                        <Select
                          name="district"
                          options={[
                            { name: "Sơn trà", value: "Son-Tra" },
                            { name: "Thanh khê", value: "Thanh-Khe" },
                          ]}
                          onChange={(value) => setFieldValue("district", value)}
                        />
                      </div>
                      <div className={cx("item")}>
                        <label htmlFor="">Phường/Xã</label>
                        <Select
                          name="ward"
                          options={[
                            { name: "Hoà Hiệp Nam", value: "Hoa-Hiep-Nam" },
                            {
                              name: "Thanh khê đông",
                              value: "Thanh-Khe-Dong",
                            },
                          ]}
                          onChange={(value) => setFieldValue("ward", value)}
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className={cx("btn-update")}>
                  <Button
                    color="orange"
                    size="block"
                    text="cập nhật"
                    onClick={() => {
                      setUpdate(!update);
                    }}
                  />
                </div>
              </Form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};

export default Info;
