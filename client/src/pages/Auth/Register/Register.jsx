import React from "react";
import { FastField, Form, Formik } from "formik";
import classNames from "classnames/bind";
import styles from "./Register.module.scss";
import { register } from "../../../config/register.config";
import CheckBox from "../../../components/common/CheckBox/CheckBox";
import Button from "../../../components/common/Button/Button";
import * as Yup from "yup";

const cx = classNames.bind(styles);
const Register = () => {
  const initialValues = {
    ho: "",
    ten: "",
    email: "",
    password: "",
    confirmPassword: "",
  };
  const validationSchema = Yup.object().shape({
    ho: Yup.string().required("Bắt buộc!"),
    ten: Yup.string().required("Bắt buộc!"),
    email: Yup.string().email("Email không hợp lệ!").required("Bắt buộc!"),
    password: Yup.string().required("Bắt buộc!"),
    confirmPassword: Yup.string()
      .oneOf([Yup.ref("password")], "Mật khẩu không trùng khớp!")
      .required("Bắt buộc!"),
  });
  return (
    <div className={cx("container")}>
      <div className={cx("register")}>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={(values) => console.log(values)}
        >
          {() => {
            return (
              <Form>
                <h1>Đăng ký tài khoản</h1>
                {register.map((item, index) => (
                  <FastField
                    key={index}
                    name={item.name}
                    component={item.component}
                    placeholder={item.placeholder}
                    icon1={item.icon1}
                    icon2={item.icon2}
                  />
                ))}
                <p>(*)Thông tin bắt buộc</p>
                <CheckBox
                  title="Đồng ý với các"
                  titleLink="điều khoản của chúng tôi"
                  link="#"
                />
                <Button size="block" color="orange" text="Đăng ký" />
              </Form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};

export default Register;
