import React from "react";
import { AiFillEye, AiFillEyeInvisible } from "react-icons/ai";
import * as Yup from "yup";

import Button from "../../../components/common/Button/Button";
import CheckBox from "../../../components/common/CheckBox/CheckBox";
import TextBox from "../../../components/common/TextBox/TextBox";
import { Link } from "react-router-dom";
import classNames from "classnames/bind";
import styles from "./Login.module.scss";
import { FastField, Form, Formik } from "formik";
const cx = classNames.bind(styles);

const Login = () => {
  const initialValues = {
    email: "",
    password: "",
  };
  const validationSchema = Yup.object().shape({
    email: Yup.string().email("Email không hợp lệ!").required("Bắt buộc!"),
    password: Yup.string().required("Bắt buộc!"),
  });

  return (
    <div className={cx("container")}>
      <div className={cx("login")}>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={(values) => console.log(values)}
        >
          {(formikProps) => {
            return (
              <Form>
                <h1>Đăng Nhập</h1>
                <FastField
                  name="email"
                  component={TextBox}
                  placeholder="email"
                />
                <FastField
                  name="password"
                  component={TextBox}
                  type="password"
                  placeholder="mật khẩu"
                  icon1={<AiFillEyeInvisible />}
                  icon2={<AiFillEye />}
                />

                <Link to="/quen-mat-khau">Quên mật khẩu?</Link>
                <CheckBox title="lưu thông tin đăng nhập" />

                <Button
                  color="orange"
                  text="đăng nhập"
                  size="block"
                  onClick={() => console.log("done")}
                />
                <p>
                  Bạn chưa có tài khoản?{" "}
                  <Link to="/register">Đăng kí ngay</Link>
                </p>
              </Form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};

export default Login;
