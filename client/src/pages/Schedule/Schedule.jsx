import React from "react";
import SlidePrize from "../../components/common/SlidePrize/SlidePrize";
import classNames from "classnames/bind";
import styles from "./Schedule.module.scss";
import { RxDoubleArrowRight } from "react-icons/rx";
import { GiTicket } from "react-icons/gi";
import { FaInfoCircle } from "react-icons/fa";
import InputSearch from "../../components/common/InputSearch/InputSearch";
import { Link } from "react-router-dom";
import Button from "../../components/common/Button/Button";
const cx = classNames.bind(styles);

const Schedule = () => {
  return (
    <>
      <SlidePrize />
      <div className={cx("container")}>
        <div className={cx("search")}>
          <InputSearch placeHolder="Tìm điểm đi ..." />
          <InputSearch placeHolder="Tìm điểm đến ..." />
        </div>
        <div className={cx("schedules")}>
          <table>
            <thead>
              <tr>
                <th>Tuyến xe</th>
                <th>Loại xe</th>
                <th>Quãng đường</th>
                <th>Thời gian hành trình</th>
                <th>Giá vé</th>
                <th>Giờ chạy</th>
                <th></th>
              </tr>
            </thead>

            <tbody>
              <tr className={cx("head")}>
                <th colSpan={7}>
                  An nhon{" "}
                  <span>
                    <RxDoubleArrowRight />
                  </span>
                </th>
              </tr>
              <tr>
                <td className={cx("placeTo")}>
                  <Link>TP. Hồ Chí Minh</Link>
                </td>
                <td>Giường</td>
                <td>600km</td>
                <td>12 giờ 46 phút</td>
                <td>200000 đồng</td>
                <td className={cx("detail")}>
                  <Link to="#">
                    Chi tiết <FaInfoCircle />
                  </Link>
                </td>
                <td>
                  <Button
                    icon={<GiTicket />}
                    text="đặt vé"
                    size="exsm"
                    color="white"
                  />
                </td>
              </tr>
            </tbody>
            <tbody>
              <tr className={cx("head")}>
                <th colSpan={7}>
                  An nhon{" "}
                  <span>
                    <RxDoubleArrowRight />
                  </span>
                </th>
              </tr>
              <tr>
                <td className={cx("placeTo")}>
                  <Link>TP. Hồ Chí Minh</Link>
                </td>
                <td>Giường</td>
                <td>600km</td>
                <td>12 giờ 46 phút</td>
                <td>200000 đồng</td>
                <td className={cx("detail")}>
                  <Link to="#">
                    Chi tiết <FaInfoCircle />
                  </Link>
                </td>
                <td>
                  <Button
                    icon={<GiTicket />}
                    text="đặt vé"
                    size="exsm"
                    color="white"
                  />
                </td>
              </tr>
            </tbody>
            <tbody>
              <tr className={cx("head")}>
                <th colSpan={7}>
                  An nhon{" "}
                  <span>
                    <RxDoubleArrowRight />
                  </span>
                </th>
              </tr>
              <tr>
                <td className={cx("placeTo")}>
                  <Link>TP. Hồ Chí Minh</Link>
                </td>
                <td>Giường</td>
                <td>600km</td>
                <td>12 giờ 46 phút</td>
                <td>200000 đồng</td>
                <td className={cx("detail")}>
                  <Link to="#">
                    Chi tiết <FaInfoCircle />
                  </Link>
                </td>
                <td>
                  <Button
                    icon={<GiTicket />}
                    text="đặt vé"
                    size="exsm"
                    color="white"
                  />
                </td>
              </tr>
            </tbody>
            <tbody>
              <tr className={cx("head")}>
                <th colSpan={7}>
                  An nhon{" "}
                  <span>
                    <RxDoubleArrowRight />
                  </span>
                </th>
              </tr>
              <tr>
                <td className={cx("placeTo")}>
                  <Link>TP. Hồ Chí Minh</Link>
                </td>
                <td>Giường</td>
                <td>600km</td>
                <td>12 giờ 46 phút</td>
                <td>200000 đồng</td>
                <td className={cx("detail")}>
                  <Link to="#">
                    Chi tiết <FaInfoCircle />
                  </Link>
                </td>
                <td>
                  <Button
                    icon={<GiTicket />}
                    text="đặt vé"
                    size="exsm"
                    color="white"
                  />
                </td>
              </tr>
            </tbody>
            <tbody>
              <tr className={cx("head")}>
                <th colSpan={7}>
                  An nhon{" "}
                  <span>
                    <RxDoubleArrowRight />
                  </span>
                </th>
              </tr>
              <tr>
                <td className={cx("placeTo")}>
                  <Link>TP. Hồ Chí Minh</Link>
                </td>
                <td>Giường</td>
                <td>600km</td>
                <td>12 giờ 46 phút</td>
                <td>200000 đồng</td>
                <td className={cx("detail")}>
                  <Link to="#">
                    Chi tiết <FaInfoCircle />
                  </Link>
                </td>
                <td>
                  <Button
                    icon={<GiTicket />}
                    text="đặt vé"
                    size="exsm"
                    color="white"
                  />
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
};

export default Schedule;
