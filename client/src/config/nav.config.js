export const nav = [
  {
    path: "/",
    title: "trang chủ",
  },
  {
    path: "/lich-trinh",
    title: "lịch trình",
  },
  {
    path: "/tin-tuc",
    title: "tin tức",
  },
  {
    path: "/tuyen-dung",
    title: "tuyển dụng",
  },
  {
    path: "/lien-he",
    title: "liên hệ",
  },

  {
    path: "/ve-chung-toi",
    title: "về chúng tôi",
  },
];
