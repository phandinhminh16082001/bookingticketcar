import slide1 from "../assets/images/slide-1.png";
import slide2 from "../assets/images/slide-2.png";
import slide3 from "../assets/images/slide-3.png";
import slide4 from "../assets/images/slide-4.png";

import slide_tin_tuc_1 from "../assets/images/slide-tin-tuc-1.png";
import slide_tin_tuc_2 from "../assets/images/slide-tin-tuc-2.png";
import slide_tin_tuc_3 from "../assets/images/slide-tin-tuc-3.png";
import slide_tin_tuc_4 from "../assets/images/slide-tin-tuc-4.png";
import slide_tin_tuc_5 from "../assets/images/slide-tin-tuc-5.png";
import slide_tin_tuc_6 from "../assets/images/slide-tin-tuc-6.png";

import slide_location_1 from "../assets/images/slide-location-1.png";
import slide_location_can_tho from "../assets/images/slide-location-can-tho.png";
import slide_location_da_nang from "../assets/images/slide-location-da-nang.png";
import slide_location_ha_noi from "../assets/images/slide-location-ha-noi.png";
import slide_location_hue from "../assets/images/slide-location-hue.png";
import slide_location_khanh_hoa from "../assets/images/slide-location-khanh-hoa.png";
import slide_location_lam_dong from "../assets/images/slide-location-lam-dong.png";
import slide_location_long_xuyen from "../assets/images/slide-location-long-xuyen.png";
import slide_location_rach_gia from "../assets/images/slide-location-rach-gia.png";
import slide_location_sai_gon from "../assets/images/slide-location-sai-gon.png";

export const slide_prize = [
  {
    img: slide1,
    path: "/tin-tuc",
  },
  {
    img: slide2,
    path: "/tin-tuc",
  },
  {
    img: slide3,
    path: "/tin-tuc",
  },
  {
    img: slide4,
    path: "/tin-tuc",
  },
];

export const slide_news = [
  {
    img: slide_tin_tuc_1,
    path: "/tin-tuc",
    title: "THƯ CẢM ƠN QUÝ HÀNH KHÁCH NHÂN DỊP XUÂN QUÝ MÃO 2023",
  },
  {
    img: slide_tin_tuc_2,
    path: "/tin-tuc",
    title: "THƯ CẢM ƠN QUÝ HÀNH KHÁCH NHÂN DỊP XUÂN QUÝ MÃO 2023",
  },
  {
    img: slide_tin_tuc_3,
    path: "/tin-tuc",
    title: "THƯ CẢM ƠN QUÝ HÀNH KHÁCH NHÂN DỊP XUÂN QUÝ MÃO 2023",
  },
  {
    img: slide_tin_tuc_4,
    path: "/tin-tuc",
    title: "THƯ CẢM ƠN QUÝ HÀNH KHÁCH NHÂN DỊP XUÂN QUÝ MÃO 2023",
  },
  {
    img: slide_tin_tuc_5,
    path: "/tin-tuc",
    title: "THƯ CẢM ƠN QUÝ HÀNH KHÁCH NHÂN DỊP XUÂN QUÝ MÃO 2023",
  },
  {
    img: slide_tin_tuc_6,
    path: "/tin-tuc",
    title: "THƯ CẢM ƠN QUÝ HÀNH KHÁCH NHÂN DỊP XUÂN QUÝ MÃO 2023",
  },
];

export const slide_locations = [
  {
    img: slide_location_1,
    location: "Cà Mau",
    name: "Năm Căn",
  },
  {
    img: slide_location_can_tho,
    location: "Cần Thơ",
  },
  {
    img: slide_location_da_nang,
    location: "Đà Nẵng",
  },
  {
    img: slide_location_ha_noi,
    location: "Hà Nội",
  },
  {
    img: slide_location_hue,
    location: "Huế",
    name: "Cố Đô Huế",
  },
  {
    img: slide_location_khanh_hoa,
    location: "Nha Trang",
    name: "Khánh Hoà",
  },
  {
    img: slide_location_lam_dong,
    location: "Đà Lạt",
    name: "Lâm Đồng",
  },
  {
    img: slide_location_long_xuyen,
    location: "Châu Đốc",
    name: "Long Xuyên",
  },
  {
    img: slide_location_rach_gia,
    location: "Hà Tiên",
    name: "Rạch Giá",
  },
  {
    img: slide_location_sai_gon,
    location: "Sài Gòn",
  },
];
