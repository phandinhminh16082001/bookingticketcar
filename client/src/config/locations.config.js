const list__locations = [
  {
    from: "Đà Nẵng",
    to: "Sài Gòn",
  },
  {
    from: "Sài Gòn",
    to: "Quy Nhơn",
  },
  {
    from: "Đà Nẵng",
    to: "Cần Thơ",
  },
  {
    from: "Đà Nẵng",
    to: "Rạch Giá",
  },
  {
    from: "Sài Gòn",
    to: "Hà Nội",
  },
  {
    from: "Khánh Hoà",
    to: "Hà Nội",
  },
  {
    from: "Huế",
    to: "Khánh Hoà",
  },
];

let listLocationFrom = [];

list__locations.forEach((item) => {
  if (!listLocationFrom.includes(item.from)) {
    return listLocationFrom.push(item.from);
  }
});

const listLocationTo = (location) => {
  let list = [];
  list__locations.forEach((item) => {
    if (item.from === location) {
      return list.push(item.to);
    }
  });
  return list;
};

export { listLocationFrom, listLocationTo };
