import MainLayout from "../layouts/MainLayout";
import Login from "../pages/Auth/Login/Login";
import Register from "../pages/Auth/Register/Register";
import History from "../pages/Auth/UserHistory/History";
import Info from "../pages/Auth/UserInfo/Info";
import Home from "../pages/Home/Home";
import News from "../pages/News/News";
import Recruitment from "../pages/Recruitment/Recruitment";
import Schedule from "../pages/Schedule/Schedule";

const routes = [
  {
    path: "/",
    element: <Home />,
    layout: MainLayout,
  },
  {
    path: "/lich-trinh",
    element: <Schedule />,
    layout: MainLayout,
  },
  {
    path: "/tin-tuc",
    element: <News />,
    layout: MainLayout,
  },
  {
    path: "/tuyen-dung",
    element: <Recruitment />,
    layout: MainLayout,
  },
  {
    path: "/login",
    element: <Login />,
    layout: MainLayout,
  },
  {
    path: "/register",
    element: <Register />,
    layout: MainLayout,
  },
  {
    path: "/user/info",
    element: <Info />,
    layout: MainLayout,
  },
  {
    path: "/user/history",
    element: <History />,
    layout: MainLayout,
  },
];

export default routes;
