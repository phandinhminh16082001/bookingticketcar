import { AiFillEye, AiFillEyeInvisible } from "react-icons/ai";
import TextBox from "../components/common/TextBox/TextBox";

export const register = [
  {
    placeholder: "họ",
    name: "ho",
    component: TextBox,
  },
  {
    placeholder: "tên",
    name: "ten",
    component: TextBox,
  },
  {
    placeholder: "email",
    name: "email",
    component: TextBox,
  },
  {
    placeholder: "mật khẩu",
    name: "password",
    icon1: <AiFillEyeInvisible />,
    icon2: <AiFillEye />,
    component: TextBox,
  },
  {
    placeholder: "xác nhận mật khẩu",
    name: "confirmPassword",
    icon1: <AiFillEyeInvisible />,
    icon2: <AiFillEye />,
    component: TextBox,
  },
];
