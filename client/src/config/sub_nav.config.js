const sub_nav = [
  {
    path: "/ve-chung-toi",
    title: "về chúng tôi",
  },
  {
    path: "/lich-trinh",
    title: "lịch trình",
  },
  {
    path: "/tin-tuc",
    title: "tin tức",
  },
  {
    path: "/tuyen-dung",
    title: "tuyển dụng",
  },
  {
    path: "/tra-cuu-thong-tin-dat-ve",
    title: "tra cứu thông tin đặt vé",
  },
  {
    path: "/dieu-khoan",
    title: "điều khoản sử dụng",
  },
  {
    path: "/hoi-dap",
    title: "hỏi đáp",
  },
  {
    path: "/huong-dan-dat-ve-web",
    title: "hướng dẫn đặt vé trên web",
  },
  {
    path: "/huong-dan-dat-ve-app",
    title: "hướng dẫn đặt vé trên app",
  },
  {
    path: "/mang-luoi-van-phong",
    title: "mạng lưới văn phòng",
  },
];
export default sub_nav;
