import React from "react";
import classNames from "classnames/bind";
import styles from "./Phone.module.scss";

const cx = classNames.bind(styles);

const Phone = () => {
  return (
    <div className={cx("wrapper")}>
      <div className={cx("ring")}>
        <div
          className={cx(
            "coccoc-alo-phone",
            "coccoc-alo-green",
            "coccoc-alo-show"
          )}
        >
          <div className={cx("coccoc-alo-ph-circle")}></div>
          <div className={cx("coccoc-alo-ph-circle-fill")}></div>
          <div className={cx("coccoc-alo-ph-img-circle")}></div>
        </div>
      </div>
    </div>
  );
};

export default Phone;
