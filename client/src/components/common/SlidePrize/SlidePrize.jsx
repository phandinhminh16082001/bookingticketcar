import React from "react";
import { slide_prize } from "../../../config/slide.config";
import Slide from "../Slide/Slide";
import classNames from "classnames/bind";
import styles from "./SlidePrize.module.scss";
import { Link } from "react-router-dom";
const cx = classNames.bind(styles);
const SlidePrize = () => {
  return (
    <div className={cx("slide-prize")}>
      <div className={cx("container")}>
        <Slide>
          {slide_prize.map((item, index) => (
            <Link to={item.path} key={index}>
              <div>
                <img src={item.img} alt="" />
              </div>
            </Link>
          ))}
        </Slide>
      </div>
    </div>
  );
};

export default SlidePrize;
