import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames/bind";
import styles from "./InputDate.module.scss";

const cx = classNames.bind(styles);

const InputDate = (props) => {
  const size = props.size ? props.size : "sm";
  return (
    <div className={cx("inputDate")}>
      <input type="date" name="date" className={cx(`${size}`)} />
    </div>
  );
};

InputDate.propTypes = {
  size: PropTypes.string,
};

export default InputDate;
