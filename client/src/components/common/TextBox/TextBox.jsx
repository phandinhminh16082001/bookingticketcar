import React, { useState } from "react";
import PropTypes from "prop-types";
import { ErrorMessage } from "formik";
import classNames from "classnames/bind";
import styles from "./TextBox.module.scss";
const cx = classNames.bind(styles);

const TextBox = (props) => {
  const { field } = props;
  const [show, setShow] = useState(false);
  const placeholder = props.placeholder ? props.placeholder : "";
  let type = props.type ? props.type : "text";
  if (props.icon2) {
    show ? (type = "text") : (type = "password");
  }
  const handleShowPassword = () => {
    setShow(!show);
  };
  return (
    <div className={cx("text-box")}>
      <input {...field} type={type} placeholder={placeholder} />
      {props.icon2 && (
        <div className={cx("icon")} onClick={handleShowPassword}>
          {show ? props.icon2 : props.icon1}
        </div>
      )}
      {field && (
        <ErrorMessage name={field.name}>
          {(msg) => <p className={cx("error")}>{msg}</p>}
        </ErrorMessage>
      )}
    </div>
  );
};
TextBox.propTypes = {
  field: PropTypes.object,
  placeholder: PropTypes.string,
  type: PropTypes.string,
};
export default TextBox;
