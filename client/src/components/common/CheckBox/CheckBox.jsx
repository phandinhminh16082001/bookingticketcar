import React from "react";
import classNames from "classnames/bind";
import PropTypes from "prop-types";
import styles from "./CheckBox.module.scss";
import { Link } from "react-router-dom";

const cx = classNames.bind(styles);

const CheckBox = (props) => {
  return (
    <label className={cx("check-box")}>
      <input type="checkbox" />
      {props.title}
      {props.titleLink && <Link to={props.link}>{props.titleLink}</Link>}
    </label>
  );
};

CheckBox.propTypes = {
  title: PropTypes.string.isRequired,
  titleLink: PropTypes.string,
  link: PropTypes.string,
};

export default CheckBox;
