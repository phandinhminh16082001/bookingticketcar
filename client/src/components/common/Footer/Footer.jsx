import React from "react";
import { Link } from "react-router-dom";
import classNames from "classnames/bind";

import DaThongBao from "../../../assets/images/DaThongBao.png";
import youtube_icon from "../../../assets/images/youtube-icon.png";
import ch_play from "../../../assets/images/ch-play.png";
import { AiFillApple, AiOutlineDoubleRight } from "react-icons/ai";
import sub_nav from "../../../config/sub_nav.config";
import footer_logos from "../../../config/footer_logos.config";
import styles from "./Footer.module.scss";

const cx = classNames.bind(styles);
const Footer = () => {
  return (
    <div className={cx("footer")}>
      <div className={cx("container")}>
        <div className={cx("footer-main")}>
          <div className={cx("info")}>
            <h2>tổng đài đặt vé và cskh</h2>
            <div className={cx("hotline")}>
              <Link>
                <h2 className={cx("phone")}>1900 6067</h2>
              </Link>
              <Link>
                <img src={DaThongBao} alt="" />
              </Link>
            </div>
            <div className={cx("address")}>
              <p>CÔNG TY CỔ PHẦN XE KHÁCH PHƯƠNG TRANG FUTA BUS LINES</p>
              <p>
                Địa chỉ: Số 01 Tô Hiến Thành, Phường 3, Thành phố Đà Lạt, Tỉnh
                Lâm Đồng, Việt Nam.
              </p>
              <p>
                Email: <Link>hotro@futa.vn</Link>
              </p>
              <div>
                <p>
                  Điện thoại: <Link>028 3838 6852</Link>
                </p>
                <p>
                  Fax: <Link>028 3838 6852</Link>
                </p>
              </div>
            </div>
          </div>
          <div className={cx("link")}>
            <div className={cx("link-item")}>
              <h2>kết nối với chúng tôi</h2>
              <Link>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="25"
                  height="25"
                  viewBox="0 0 28 28"
                  data-v-fc619bc0=""
                >
                  <g fill="none" data-v-fc619bc0="">
                    <circle
                      cx="13.9"
                      cy="13.9"
                      r="13.9"
                      fill="#3B5998"
                      data-v-fc619bc0=""
                    ></circle>{" "}
                    <path
                      fill="#FFF"
                      d="M17.395 14.445h-2.48v9.086h-3.758v-9.086H9.369V11.25h1.788V9.184c0-1.477.702-3.791 3.791-3.791l2.784.011v3.1h-2.02c-.331 0-.797.166-.797.87v1.88h2.808l-.328 3.19z"
                      data-v-fc619bc0=""
                    ></path>
                  </g>
                </svg>
              </Link>
              <Link>
                <img src={youtube_icon} alt="" />
              </Link>
            </div>
            <div className={cx("link-item")}>
              <h2>tải app futa</h2>
              <div className={cx("content")}>
                <Link>
                  <div className={cx("item", "ios")}>
                    <AiFillApple />
                    <span className={cx("title")}>app store</span>
                  </div>
                </Link>
                <Link>
                  <div className={cx("item", "android")}>
                    <img src={ch_play} alt="" />
                    <span className={cx("title")}>ch play</span>
                  </div>
                </Link>
              </div>
            </div>
          </div>
          <div className={cx("sub-nav")}>
            <h2>FUTA Bus Lines</h2>
            <div className={cx("sub-nav-content")}>
              {sub_nav.map((item, index) => (
                <Link to={item.path} key={index}>
                  <div className={cx("item")}>
                    <div className={cx("item__icon")}>
                      <AiOutlineDoubleRight />
                    </div>
                    <p className={cx("item__title")}>{item.title}</p>
                  </div>
                </Link>
              ))}
            </div>
          </div>
          <div className={cx("service")}>
            {footer_logos.map((logo, index) => (
              <Link to={logo.path} key={index}>
                <div className={cx("image")}>
                  <img src={logo.logo} alt="" />
                </div>
              </Link>
            ))}
          </div>
          <p className={cx("text")}>
            © 2023 | Bản quyền thuộc về Công ty Cổ Phần Xe Khách Phương Trang
            FUTA Bus Lines | www.futabus.vn | Chịu trách nhiệm nội dung: Tào
            Thành Danh
          </p>
        </div>
      </div>
    </div>
  );
};

export default Footer;
