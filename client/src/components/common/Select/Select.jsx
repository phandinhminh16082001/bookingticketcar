import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames/bind";
import styles from "./Select.module.scss";

const cx = classNames.bind(styles);

const Select = (props) => {
  const onChange = (e) => {
    if (props.onChange) {
      props.onChange(e.target.value);
    }
  };
  return (
    <select
      name={props.name}
      className={cx("select", `${props.size}`)}
      onChange={onChange}
    >
      {props.placeholder && (
        <option value="" selected hidden>
          {props.placeholder}
        </option>
      )}
      {props.options.map((item, index) => (
        <option value={item.value} key={index}>
          {item.name}
        </option>
      ))}
    </select>
  );
};

Select.propTypes = {
  name: PropTypes.string,
  onChange: PropTypes.func,
  options: PropTypes.array,
  placeholder: PropTypes.string,
  size: PropTypes.string,
};

export default Select;
