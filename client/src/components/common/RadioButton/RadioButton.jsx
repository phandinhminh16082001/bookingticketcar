import React from "react";
import PropTypes from "prop-types";
import styles from "./RadioButton.module.scss";
import classNames from "classnames/bind";
const cx = classNames.bind(styles);

const RadioButton = (props) => {
  const { field } = props;
  const checked = props.checked || false;
  const onChange = (e) => {
    props.onChange(e.target.value);
  };
  return (
    <label className={cx("radio")}>
      <input
        type="radio"
        onChange={onChange}
        name={field ? field.name : props.nameRb}
        {...field}
        value={props.value}
        defaultChecked={checked}
      />
      {props.text}
    </label>
  );
};

RadioButton.propTypes = {
  name: PropTypes.string,
  text: PropTypes.string,
  value: PropTypes.string,
};

export default RadioButton;
