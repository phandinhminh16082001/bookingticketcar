import React from "react";
import classNames from "classnames/bind";
import styles from "./Header.module.scss";
import { BsFillTelephoneFill } from "react-icons/bs";
import { IoMdMail } from "react-icons/io";
import { CgProfile } from "react-icons/cg";
import { Link, NavLink, useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { icons, iconTranslate } from "../../../config/icon.config";
import Button from "../Button/Button";
import logo from "../../../assets/images/logo.png";
import { nav } from "../../../config/nav.config";

const cx = classNames.bind(styles);

const Header = () => {
  const navigate = useNavigate();
  const { t, i18n } = useTranslation("home");
  return (
    <div className={cx("header")}>
      <div className={cx("top-bar")}>
        <div className={cx("container")}>
          <div className={cx("top-bar-main")}>
            <div className={cx("info")}>
              <div className={cx("item")}>
                <div className={cx("icon")}>
                  <BsFillTelephoneFill />
                </div>
                <div className={cx("text")}>19006067</div>
              </div>
              <div className={cx("item")}>
                <div className={cx("icon")}>
                  <IoMdMail />
                </div>
                <div className={cx("text")}>hotro@futa.vn</div>
              </div>
            </div>
            <div className={cx("link")}>
              {icons.map((icon, index) => (
                <Link to={icon.path} key={index}>
                  <span className={cx("link-icon")}>
                    {icon.icon ? icon.icon : ""}
                  </span>
                  {icon.img ? <img src={icon.img} alt="" /> : ""}
                  <span className={cx("link-title")}>
                    {icon.title ? icon.title : ""}
                  </span>
                </Link>
              ))}
              {iconTranslate.map((item, index) => (
                <Button
                  text={item.title}
                  icon={item.icon}
                  color="transparent"
                  key={index}
                  onClick={() => i18n.changeLanguage(item.lng)}
                />
              ))}
              <Button
                icon={<CgProfile />}
                text={t("header.đăng nhập")}
                color="red"
                size="sm"
                onClick={() => navigate("/login")}
              />
            </div>
          </div>
        </div>
      </div>
      <div className={cx("nav")}>
        <div className={cx("container")}>
          <div className={cx("nav-main")}>
            <div className={cx("nav-icon")}>
              <img src={logo} alt="" />
            </div>
            {nav.map((item, index) => (
              <NavLink
                to={item.path}
                key={index}
                className={({ isActive }) => (isActive ? "active" : undefined)}
              >
                {t(`header.${item.title}`)}
              </NavLink>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
