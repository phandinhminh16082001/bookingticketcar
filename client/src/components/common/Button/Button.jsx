import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames/bind";
import styles from "./Button.module.scss";

let cx = classNames.bind(styles);

const Button = (props) => {
  const color = props.color ? `bg-${props.color}` : "bg-orange";
  const size = props.size ? `btn-${props.size}` : "btn-sm";

  const text = props.text ? props.text : "";
  return (
    <button
      type="submit"
      className={cx("btn", `${size}`, `${color}`)}
      onClick={props.onClick ? () => props.onClick() : null}
    >
      {props.icon && <div className={cx("icon")}>{props.icon}</div>}
      <div className={cx("text")}>{text}</div>
    </button>
  );
};

Button.propTypes = {
  color: PropTypes.string,
  size: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func,
};

export default Button;
