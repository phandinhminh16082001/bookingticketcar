import React from "react";
import Slider from "react-slick";

const Slide = (props) => {
  const centerMode = props.centerMode ? props.centerMode : false;
  const className = props.className ? props.className : "";
  const slidesToShow = props.slidesToShow ? props.slidesToShow : 1;
  const settings = {
    className: className,
    centerMode: centerMode,
    infinite: true,
    speed: 500,
    autoplaySpeed: 2000,
    autoplay: true,
    slidesToShow: slidesToShow,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 400,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
        },
      },
    ],
  };
  return <Slider {...settings}>{props.children}</Slider>;
};

export default Slide;
