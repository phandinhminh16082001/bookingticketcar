import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames/bind";
import styles from "./InputSearch.module.scss";
import Button from "../Button/Button";
import { AiOutlineSearch } from "react-icons/ai";
const cx = classNames.bind(styles);
const InputSearch = (props) => {
  const placeHolder = props.placeHolder ? props.placeHolder : "";
  const showIcon = props.showIcon && true;
  const size = props.size ? props.size : "";
  return (
    <div className={cx("inputSearch")}>
      <input
        className={cx(`${size}`)}
        type="search"
        placeholder={placeHolder}
        name="search"
      />
      {showIcon && (
        <div className={cx("btn-search")}>
          <Button icon={<AiOutlineSearch />} color="white" />
        </div>
      )}
    </div>
  );
};

InputSearch.propTypes = {
  placeHolder: PropTypes.string,
};

export default InputSearch;
