import React, { useState } from "react";

import moment from "moment";
import Button from "../../common/Button/Button";
import { AiOutlineSearch } from "react-icons/ai";
import Autocomplete from "@mui/material/Autocomplete";
import TextField from "@mui/material/TextField";
import change__img from "../../../assets/images/change.png";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { DemoContainer } from "@mui/x-date-pickers/internals/demo";

import classNames from "classnames/bind";
import styles from "./Search.module.scss";
import RadioButton from "../../common/RadioButton/RadioButton";
import {
  listLocationFrom,
  listLocationTo,
} from "../../../config/locations.config";
const cx = classNames.bind(styles);

const Search = (props) => {
  const initInfoSearch = {
    type: "",
    from: "",
    to: "",
    dateStart: "",
    dateEnd: "",
  };
  const [from, setFrom] = useState("");
  const [show, setShow] = useState(true);
  const [infoSearch, setInfoSearch] = useState(initInfoSearch);
  const fillterSelected = (type, value) => {
    switch (type) {
      case "ticket": {
        setInfoSearch({ ...infoSearch, type: value });
        setShow(value !== "khứ hồi");
        break;
      }
      case "placeFrom": {
        setInfoSearch({ ...infoSearch, from: value });
        setFrom(value);
        break;
      }
      case "placeTo":
        setInfoSearch({ ...infoSearch, to: value });
        break;
      case "dateStart":
        setInfoSearch({ ...infoSearch, dateStart: value });
        break;
      case "dateEnd":
        setInfoSearch({ ...infoSearch, dateEnd: value });
        break;
      default:
        break;
    }
  };
  console.log(infoSearch);
  return (
    <div className={cx("container")}>
      <div className={cx("search")}>
        <div className={cx("roundtrip")}>
          <RadioButton
            value="một chiều"
            nameRb="rd"
            text="một chiều"
            onChange={(value) => fillterSelected("ticket", value)}
            checked={true}
          />
          <RadioButton
            value="khứ hồi"
            nameRb="rd"
            text="khứ hồi"
            onChange={(value) => fillterSelected("ticket", value)}
          />
        </div>
        <div className={cx("booking")}>
          <div className={cx("place")}>
            <div className={cx("place-from")}>
              <h2>Điểm đi</h2>
              <Autocomplete
                disablePortal
                freeSolo
                disableCloseOnSelect
                id="combo-box-demo"
                options={listLocationFrom}
                onChange={(e, value) => fillterSelected("placeFrom", value)}
                // getOptionLabel={(option) => option.from}
                sx={{
                  "& .MuiOutlinedInput-root": {
                    borderRadius: "0",
                    padding: "0",
                    fontSize: 20,
                  },
                  "& .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline": {
                    border: "none",
                  },
                  "& .css-md26zr-MuiInputBase-root-MuiOutlinedInput-root": {
                    fontSize: 20,
                  },
                  "& .MuiOutlinedInput-root .MuiAutocomplete-input": {
                    padding: "0px",
                  },
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    placeholder="Chọn điểm đi"
                    variant="outlined"
                  />
                )}
              />
            </div>
            <img src={change__img} alt="change" />
            <div className={cx("place-to")}>
              <h2>Điểm đến</h2>

              <Autocomplete
                disablePortal
                freeSolo
                disableCloseOnSelect
                id="combo-box-demo"
                options={listLocationTo(from)}
                onChange={(e, value) => fillterSelected("placeTo", value)}
                // getOptionLabel={(option) => option.from}
                sx={{
                  "& .MuiOutlinedInput-root": {
                    borderRadius: "0",
                    padding: "0",
                    fontSize: 20,
                  },
                  "& .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline": {
                    border: "none",
                  },
                  "& .css-md26zr-MuiInputBase-root-MuiOutlinedInput-root": {
                    fontSize: 20,
                  },
                  "& .MuiAutocomplete-inputRoot .MuiAutocomplete-input": {
                    padding: "0px",
                  },
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    placeholder="Chọn điểm đi"
                    variant="outlined"
                    sx={{ fontSize: 20 }}
                  />
                )}
              />
            </div>
          </div>
          <div className={cx("date")}>
            <div className={cx("date-from")}>
              <h2>Ngày Đi</h2>
              <div className={cx("date-input")}>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DemoContainer components={["DatePicker"]}>
                    <DatePicker
                      disablePast={true}
                      sx={{
                        "& .css-o9k5xi-MuiInputBase-root-MuiOutlinedInput-root":
                          {
                            fontSize: 14,
                          },
                        "& .css-nxo287-MuiInputBase-input-MuiOutlinedInput-input":
                          {
                            padding: "5px 10px",
                          },
                      }}
                      onChange={(value) =>
                        fillterSelected(
                          "dateStart",
                          moment(new Date(value)).format("DD/MM/YYYY")
                        )
                      }
                      format="DD/MM/YYYY"
                    />
                  </DemoContainer>
                </LocalizationProvider>
              </div>
            </div>
            <div className={cx("date-to")}>
              <h2>Ngày Về</h2>
              <div className={cx("date-input")}>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DemoContainer components={["DatePicker"]}>
                    <DatePicker
                      disablePast={true}
                      sx={{
                        "& .css-o9k5xi-MuiInputBase-root-MuiOutlinedInput-root":
                          {
                            fontSize: 14,
                          },
                        "& .css-nxo287-MuiInputBase-input-MuiOutlinedInput-input":
                          {
                            padding: "5px 10px",
                          },
                      }}
                      onChange={(value) =>
                        fillterSelected(
                          "dateEnd",
                          moment(new Date(value)).format("DD/MM/YYYY")
                        )
                      }
                      format="DD/MM/YYYY"
                      disabled={show}
                    />
                  </DemoContainer>
                </LocalizationProvider>
              </div>
            </div>
          </div>
        </div>
        <div className={cx("search-btn")}>
          <Button
            color="orange"
            size="sm"
            text="tìm chuyến xe"
            icon={<AiOutlineSearch />}
          />
        </div>
      </div>
    </div>
  );
};

export default Search;
